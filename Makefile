requirements:
	direnv allow .
	pip install -U -r requirements.txt --no-cache

keypair:
	ssh-keygen -t rsa -b 4096 -f keys/ansible.key -N "" -C "taz@aws"

init-network:
	bash -c "cd ./terraform/network && terraform init"

init-compute:
	bash -c "cd ./terraform/compute && terraform init"

apply-network:
	bash -c "cd ./terraform/network && terraform apply -auto-approve -input=false -refresh=true"

apply-compute:
	bash -c "cd ./terraform/compute && terraform apply -auto-approve -input=false -refresh=true"
	bash -c "cd ./terraform/compute && terraform output ssh_config > ../../ssh.cfg"

destroy-network:
	bash -c "cd ./terraform/network && terraform destroy -auto-approve -input=false -refresh=true"

destroy-compute:
	bash -c "cd ./terraform/compute && terraform destroy -auto-approve  -input=false -refresh=true"
