locals {
  vpc_cidr = "10.42.0.0/16"
  subnet_cidr = "10.42.42.0/24"
}
resource "aws_vpc" "taz" {
    cidr_block = local.vpc_cidr
}

resource "aws_route_table" "sink" {
  vpc_id = aws_vpc.taz.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig-main.id
  }
}

resource "aws_route_table_association" "eu-west-1a-public" {
  route_table_id = aws_route_table.sink.id
  subnet_id      =  aws_subnet.taz.id
}

resource "aws_subnet" "taz" {
  vpc_id            = aws_vpc.taz.id
  cidr_block        = local.subnet_cidr
  availability_zone = "${var.aws_region}a"
}

resource "aws_internet_gateway" "ig-main" {
  vpc_id = aws_vpc.taz.id
}