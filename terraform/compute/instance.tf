locals {
  private_key_file = "${path.module}/../../keys/ansible.key"
  public_key_file = "${local.private_key_file}.pub"
  dns_taz_domain = "${var.dns_taz_subdomain}.${var.dns_parent_route53_public_zone}"
}

resource "aws_key_pair" "ansible" {
  key_name   = var.aws_key_name
  public_key = file(local.public_key_file)
}

resource "aws_instance" "taz" {
  ami = data.aws_ami.debian.id
  instance_type = var.instance_type
  key_name = aws_key_pair.ansible.key_name
  vpc_security_group_ids = [aws_security_group.taz.id]
  subnet_id = data.terraform_remote_state.vpc.outputs.public_subnet_id
  source_dest_check = false

  root_block_device {
    volume_size = var.root_block_device_size
    volume_type = var.root_block_device_type
  }

  user_data = <<-EOF
#cloud-config
write_files:
  - path: "/etc/ansible/hosts"
    content: |-
      [clans]
      localhost ansible_connection=local
  - path: "/etc/ansible/host_vars/localhost.yml"
    content: |-
      ---
      system_base_domain: "${local.dns_taz_domain}"
      rproxy_enable: yes
      chat_enable: yes
      videoconf_enable: yes
      dns_enable: yes
      mailserver_enable: yes
      mattermost_enable_smtp_auth: yes
      mattermost_smtp_host: "mail.{{ system_base_domain }}"
      mattermost_smtp_port: "465"
      mattermost_smtp_user: "hostmaster@{{ system_base_domain }}"
      mattermost_smtp_pass: "{{ lookup('file', '/root/galaxie-clans/secrets/system_users/localhost/autobot.password') }}"
      rproxy_nginx_managed_sites:
        - "mattermost"
        - "jitsi-meet"
      bind_listen_ipv6:
        - "none"
      bind_zone_domains:
        - name: "{{ system_base_domain }}"
          type: master
          hostmaster_email: "hostmaster.{{ system_base_domain }}"
          allow_update:
            - "localhost"
          name_servers:
            - "ns"
          mail_servers:
            - name: "mail"
              preference: 10
          hosts:
            - name: ""
              ip: "${aws_eip.taz.public_ip}"
              ttl: 300
            - name: "ns"
              ip: "${aws_eip.taz.public_ip}"
              ttl: 300
            - name: "mail"
              ip: "${aws_eip.taz.public_ip}"
              ttl: 300
              aliases:
                - "chat"
                - "meet"
                - "stun"
      acme_account_email: "hostmaster@{{ system_base_domain }}"
      acme_domains:
        - cn: "{{ system_base_domain }}"
          zone: "{{ system_base_domain }}"
          alt:
            - "DNS:chat.{{ system_base_domain }}"
            - "DNS:mail.{{ system_base_domain }}"
            - "DNS:meet.{{ system_base_domain }}"
            - "DNS:stun.{{ system_base_domain }}"
            
packages:
  - apt-transport-https
  - aptitude
  - build-essential
  - git
  - libffi-dev
  - libperl-dev
  - libssl-dev
  - python3
  - python3-apt
  - python3-distutils-extra
  - python3-venv
  - python3-pip
  - python3-setuptools
  - python-apt-dev
  - direnv
  - sudo
runcmd:
  - "git clone https://gitlab.com/Tuuux/galaxie-clans.git /root/galaxie-clans"
  - "cd /root/galaxie-clans && make dependencies"
  - "cd /root/galaxie-clans && ansible-playbook playbooks/setup_core_services.yml -e scope=localhost -i /etc/ansible/hosts"
  - "cd /root/galaxie-clans && ansible-playbook playbooks/acme_rotate_certificates.yml -e scope=localhost -i /etc/ansible/hosts"
  - "cd /root/galaxie-clans && ansible-playbook playbooks/setup_broadcast_services.yml -e scope=localhost -i /etc/ansible/hosts"
EOF

  tags = {
    Name = local.dns_taz_domain
  }
}

resource "aws_route53_record" "subdomain_a" {
  zone_id = data.aws_route53_zone.wescalefr.zone_id
  name    = "ns.${local.dns_taz_domain}"
  type    = "A"
  ttl     = "5"

  records = [
    aws_eip.taz.public_ip
  ]
}

resource "aws_route53_record" "subdomain_ns" {
  zone_id = data.aws_route53_zone.wescalefr.zone_id
  name    = local.dns_taz_domain
  type    = "NS"
  ttl     = "5"

  records = [
    "ns.${local.dns_taz_domain}"
  ]
}

resource "aws_eip" "taz" {
  vpc = true
}

resource "aws_eip_association" "taz" {
  instance_id   = aws_instance.taz.id
  allocation_id = aws_eip.taz.id
}